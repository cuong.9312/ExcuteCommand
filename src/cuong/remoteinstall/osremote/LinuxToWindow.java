package cuong.remoteinstall.osremote;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class LinuxToWindow extends OperatingSystemBase {
	public LinuxToWindow() {
	}

	public LinuxToWindow(String user, String pass, String ipHost) {
		super(user, pass, ipHost);
	}

	public boolean dirC() {
		try {
			Process proc = new ProcessBuilder("bash", "-c",
//					"winexe -U WORKGROUP/Administrator%1 //10.15.152.143 'cmd /C cd C:\\ & dir'").start();
					"winexe -U WORKGROUP/Administrator%1 //10.15.152.143 'cmd /C ftp 127.0.0.1'").start();

			// Read the output

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				System.out.print(line + "\n");
			}

			proc.waitFor();
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

}
