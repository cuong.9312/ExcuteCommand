package cuong.remoteinstall.osremote;

public abstract class OperatingSystemBase {
	//tài khoản đang nhập vào client
	String user;
	String pass;
	
	//địa chỉ ip của client
	String ipHost;

	public OperatingSystemBase() {
	}

	public OperatingSystemBase(String user, String pass, String ipHost) {
		this.user = user;
		this.pass = pass;
		this.ipHost = ipHost;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getIpHost() {
		return ipHost;
	}

	public void setIpHost(String ipHost) {
		this.ipHost = ipHost;
	}
	
}
