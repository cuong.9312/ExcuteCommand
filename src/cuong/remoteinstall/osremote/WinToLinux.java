package cuong.remoteinstall.osremote;

import java.io.InputStream;
import java.io.OutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/*
 * Client linux need to install ssh before command
 * */
public class WinToLinux extends OperatingSystemBase {
	public WinToLinux() {
	}

	public WinToLinux(String user, String pass, String ipHost) {
		super(user, pass, ipHost);
	}

	// folder: thÆ° má»¥c chá»©a file cáº§n xÃ³a
	// file: tÃªn file cáº§n xÃ³a
	public boolean removesFile(String folder, String file) {
		try {
			// Ä‘á»ƒ thá»±c hiá»‡n nhiá»�u lá»‡nh: sau má»—i lá»‡nh ta thÃªm \n
			String command = "cd " + folder + "\n rm " + file + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: thÆ° má»¥c cha chá»©a thÆ° má»¥c cáº§n xÃ³a:
	// folderDelete: tÃªn thÆ° má»¥c cáº§n xÃ¡o
	public boolean removesFolder(String folder, String folderDelete) {
		try {
			String command = "cd " + folder + "\n rm -rf " + folderDelete + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: thÆ° má»¥c cha chá»©a thÆ° má»¥c cáº§n tao:
	// folderCreate: tÃªn thÆ° má»¥c cáº§n tao
	public boolean mkdir(String folder, String folderCreate) {
		try {
			String command = "cd " + folder + "\n mkdir " + folderCreate + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean downloadFile(String folder, String url) {
		try {
			System.out.println("start downloading file");
			String command = "cd " + folder + "\n wget " + url + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// nhá»¯ng lá»‡nh cáº§n thá»±c hiá»‡n vá»›i quyá»�n admin (sudo) cáº§n cÃ³ thÃªm 1 bÆ°á»›c truyá»�n
	// pass word khi thá»±c hiá»‡n
	public boolean sudo() {
		try {
			// Ä‘á»ƒ thá»±c hiá»‡n nhiá»�u lá»‡nh: sau má»—i lá»‡nh ta thÃªm \n
			String command = "sudo apt-get update";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y\n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean install(String namePackage) {
		try {
			// Ä‘á»ƒ thá»±c hiá»‡n nhiá»�u lá»‡nh: sau má»—i lá»‡nh ta thÃªm \n
			String command = "sudo apt-get install " + namePackage;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean uninstall(String namePackage) {
		try {
			// Ä‘á»ƒ thá»±c hiá»‡n nhiá»�u lá»‡nh: sau má»—i lá»‡nh ta thÃªm \n
			String command = "sudo apt-get remove " + namePackage;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

}
