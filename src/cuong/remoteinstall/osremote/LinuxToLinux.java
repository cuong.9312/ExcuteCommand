package cuong.remoteinstall.osremote;

import java.io.InputStream;
import java.io.OutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class LinuxToLinux extends OperatingSystemBase {
	public LinuxToLinux() {
	}

	public LinuxToLinux(String user, String pass, String ipHost) {
		super(user, pass, ipHost);
	}

	// folder: thư mục chứa file cần xóa
	// file: tên file cần xóa
	public boolean removesFile(String folder, String file) {
		try {
			// để thực hiện nhiều lệnh: sau mỗi lệnh ta thêm \n
			String command = "cd " + folder + "\n rm " + file + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out
					.println("loi WinToLinux.RemovesFile: " + ex.getMessage());
			return false;
		}
	}

	// folder: thư mục cha chứa thư mục cần xóa:
	// folderDelete: tên thư mục cần xáo
	public boolean removesFolder(String folder, String folderDelete) {
		try {
			String command = "cd " + folder + "\n rm -rf " + folderDelete + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out.println("loi WinToLinux.RemovesFolder: "
					+ ex.getMessage());
			return false;
		}
	}

	// folder: thư mục cha chứa thư mục cần tao:
	// folderCreate: tên thư mục cần tao
	public boolean mkdir(String folder, String folderCreate) {
		try {
			String command = "cd " + folder + "\n mkdir " + folderCreate + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out.println("loi WinToLinux.mkdir: "
					+ ex.getMessage());
			return false;
		}
	}

	public boolean downloadFile(String folder, String url) {
		try {
			String command = "cd " + folder + "\n wget " + url + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out.println("loi WinToLinux.RemovesFolder: "
					+ ex.getMessage());
			return false;
		}
	}

	// những lệnh cần thực hiện với quyền admin (sudo) cần có thêm 1 bước truyền
	// pass word khi thực hiện
	public boolean sudo() {
		try {
			// để thực hiện nhiều lệnh: sau mỗi lệnh ta thêm \n
			String command = "sudo apt-get update";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y\n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out
					.println("loi WinToLinux.RemovesFile: " + ex.getMessage());
			return false;
		}
	}

	public boolean install(String namePackage) {
		try {
			// để thực hiện nhiều lệnh: sau mỗi lệnh ta thêm \n
			String command = "sudo apt-get install " + namePackage;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out
					.println("loi WinToLinux.RemovesFile: " + ex.getMessage());
			return false;
		}
	}

	public boolean uninstall(String namePackage) {
		try {
			// để thực hiện nhiều lệnh: sau mỗi lệnh ta thêm \n
			String command = "sudo apt-get remove " + namePackage;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			System.out
					.println("loi WinToLinux.RemovesFile: " + ex.getMessage());
			return false;
		}
	}
}
