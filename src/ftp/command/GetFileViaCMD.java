package ftp.command;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class GetFileViaCMD {
	public static void main(String[] args) {
		getFile("10.15.152.143","cuong.txt");
	}

	//login with account
	public static boolean getFile(String server, String user, String password,
			String fileDownload) {
		try {
			String commandLine = "echo open " + server + " >> ftp &echo user "
					+ user + " " + password
					+ " >> ftp &echo binary >> ftp &echo get " + fileDownload
					+ " >> ftp &echo bye >> ftp &ftp -n -v -s:ftp &del ftp";
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					commandLine);

			// execute command line
			Process p = builder.start();
			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("downloaded!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	
	//Anonoymous ftp login
	public static boolean getFile(String server, String fileDownload) {
		try {
			String commandLine = "echo open " + server+ " >> ftp &echo user anonymous "
					+ " >> ftp &echo binary >> ftp &echo get " + fileDownload
					+ " >> ftp &echo bye >> ftp &ftp -n -v -s:ftp &del ftp";
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					commandLine);

			// execute command line
			Process p = builder.start();
			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("downloaded!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public static String readStream(InputStream stream) {
		try {
			return IOUtils.toString(stream);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			if (stream != null)
				IOUtils.closeQuietly(stream);
		}
	}
}
