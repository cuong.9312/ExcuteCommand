package com.fis.cdd.remote.model;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

/*
 * to use psExec in psTool to execute command to client
 * we need copy file psexec.exe into C:\windows\system32
 * or copy file psexec.exe into project
 * */
public class WinToWin extends OperatingSystemBase {
	public WinToWin() {
	}

	public WinToWin(String user, String pass, String ipHost) {
		super(user, pass, ipHost);
	}

	public static void main(String[] args) {
		WinToWin wtw = new WinToWin("administrator", "cuong", "10.15.152.103");
		// wtw.sendFile("store\\CDDScanner-v1.0-Full.jar");
		String folder = "C:\\my_install";
		// wtw.creatDir(folder);
		// wtw.getFileViaFtp("10.15.152.143", "scannerConfig.xml", folder);
		// wtw.getFileViaFtp("10.15.152.143", "CDDScanner-v1.0-Full.jar",
		// folder);
		wtw.runFileJar(folder, "CDDScanner-v1.0-Full.jar");
		// wtw.deleteFolder("C:\\my_install");
		// wtw.getFileViaFtp("127.0.0.1", "cuong", "admin1234", "cuong.txt",
		// "C:\\cuong3");
		// "C:\\install");
		// wtw.getFileViaFtp("10.15.152.143", "jre.exe",
		// "C:\\install");
		// wtw.sendFile("acuong.txt");
		// wtw.sendFile("scannerConfig.xml");
		// wtw.runFileJar("C:\\windows", "CDDScanner-v1.0-Full.jar");
		// if (wtw.isConnect()) {
		//
		// System.out.println("not found error");
		// } else {
		// System.out.println("found error:");
		// System.out.println(wtw.getStatus());
		// }
	}

	// folder: folder contain file need to delete (url)
	// file: file to delete (contains extends of file: .exe, .txt ..)
	public boolean removeFile(String folder, String file) {
		try {

			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " -w " + folder + " cmd /c \"del \""
							+ file + " \"\"");

			// execute command line
			Process p = builder.start();
			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("deleted!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: folder delete (url)
	public boolean removeFolder(String folder) {
		try {

			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " cmd /c \" RD /S /Q " + folder + "  \"");

			// execute command line
			Process p = builder.start();
			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("deleted!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: folder contain file run
	// fileRun: file run
	// arg: main arg when run file .jar
	public boolean runFileJar(String folder, String fileRun, String arg) {
		try {
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " -w " + folder + "java -jar " + fileRun
							+ " " + arg + " ");
			// ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
			// "psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
			// + pass + " -w " + folder
			// + " cmd /s /k \"java -jar " + fileRun + " " + arg
			// + " \"");

			// execute command line
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("run!");
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean runFileJar(String folder, String fileRun) {
		try {
			System.out.println("start run file jar : " + fileRun + "  on "
					+ this.ipHost);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " -w " + folder + " java -jar " + fileRun);
			// ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
			// "psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
			// + pass + " -w " + folder
			// + " cmd /s /k \"java -jar " + fileRun + "\"");

			// execute command line
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("finish run!");
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: folder contain file run
	// fileRun: file run
	public boolean copyAndRunFile(String folder, String fileCopy) {
		try {
			// file copy is stored at C:\window of client
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " -c " + fileCopy + "");

			// cd to folder contains file run
			builder.directory(new File(folder));
			// builder.redirectErrorStream(true);

			// thực thi command line
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("copied!");
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// before run sendFile, you have to copy file copy into project
	public boolean sendFile(String fileCopy) {
		try {
			System.out.println("Start sending: " + fileCopy);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " -c " + fileCopy + "");

			// execute command
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("File sent to " + ipHost);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean createFolder(String dir) {
		try {
			System.out.println("Start creat: " + dir);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " cmd /c \" md " + dir + "\"");

			// execute command
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("created  " + dir + "  on " + ipHost);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// login ftp server with account
	public boolean getFileViaFtp(String serverFtp, String userFtp,
			String passwordFtp, String fileDownload, String folderStore) {
		try {
			System.out.println("start download from ftp server...");

			String commandLine = "echo open " + serverFtp
					+ " >> ftp &echo user " + userFtp + " " + passwordFtp
					+ " >> ftp &echo binary >> ftp &echo get " + fileDownload
					+ " >> ftp &echo bye >> ftp &ftp -n -v -s:ftp &del ftp";
			String psexecCommand = "psexec -i -d \\\\" + ipHost + " -u " + user
					+ " -p " + pass + " -w " + folderStore + " cmd /c \" "
					+ commandLine + " \"";
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					psexecCommand);

			// execute command line
			Process p = builder.start();
			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("downloaded!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// login ftp server with anonymous
	public boolean getFileViaFtp(String serverFtp, String fileDownload,
			String folderStore) {
		try {
			System.out.println("start download from ftp server...");

			String commandLine = "echo open " + serverFtp
					+ " >> ftp &echo user anonymous  "
					+ " >> ftp &echo binary >> ftp &echo get " + fileDownload
					+ " >> ftp &echo bye >> ftp &ftp -n -v -s:ftp &del ftp";
			String psexecCommand = "psexec -i -d \\\\" + ipHost + " -u " + user
					+ " -p " + pass + " -w " + folderStore + " cmd /c \" "
					+ commandLine + " \"";
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					psexecCommand);

			// execute command line
			Process p = builder.start();
			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			System.out.println("downloaded!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// before run sendFile, you have to copy file copy into project
	public boolean sendFile(String folder, String fileCopy) {
		try {
			System.out.println("Start sending: " + fileCopy);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " -c " + fileCopy + "");

			// execute command
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);

			System.out.println("File sent to " + ipHost);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean isConnect() {
		try {
			System.out.println("Start test connect client: ");
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c",
					"psexec -i -d \\\\" + ipHost + " -u " + user + " -p "
							+ pass + " ipconfig");

			// execute command
			Process p = builder.start();

			// get result from client's command line
			String success = readStream(p.getInputStream());
			String error = readStream(p.getErrorStream());
			System.out.println(success);
			System.out.println(error);
			if (error.contains("Logon failure")
					|| error.contains("Couldn't access")) {
				status = error;
				return false;
			}
			System.out.println("finished test connect");
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public static String readStream(InputStream stream) {
		try {
			return IOUtils.toString(stream);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			if (stream != null)
				IOUtils.closeQuietly(stream);
		}
	}
}
