package com.fis.cdd.remote.model;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/*
 * Client linux need to install ssh before command
 * sudo apt-get install openssh-server
 * */
public class WinToLinux extends OperatingSystemBase {
	public WinToLinux() {
	}

	public WinToLinux(String user, String pass, String ipHost) {
		super(user, pass, ipHost);
	}

	public static void main(String[] args) {
		WinToLinux wtl = new WinToLinux("mayao01", "123456", "10.15.152.78");
		wtl.getFileViaFtp("10.15.152.143", "jre.exe", "/home/" + wtl.getUser() + "/Desktop");
		
//		wtl.createFolder("/home/" + wtl.getUser() + "/Desktop", "test");
//		wtl.createFolder("/home/" + wtl.getUser() + "/Desktop/test2");
		// wtl.downloadFile("/home/"+wtl.getUser()+"/Desktop/test",
		// "http://10.15.152.60:8080/test/test.jar");
		// wtl.runFileJar("/home/" + wtl.getUser() + "/Desktop/test",
		// "hello.jar");
		// wtl.removesFolder("/home/"+wtl.getUser()+"/Desktop", "test");
		// wtl.removesFile("/home/" + wtl.getUser() + "/Desktop", "test.txt");
	}

	// folder: contains file to delete
	// file: file to delete
	public boolean removeFile(String folder, String file) {
		try {
			// to execute multi command, we add \n after each commandline
			String command = "cd " + folder + "\n rm " + file + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("removed file!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: folder contain file .jar
	// file: file to run
	public boolean runFileJar(String folder, String file) {
		try {
			String command = "cd " + folder + "\n java -jar  " + file + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("run file!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder: folder contain folder to delete
	// folderDelete: folder to delete
	public boolean removesFolder(String folder, String folderDelete) {
		try {
			String command = "cd " + folder + "\n rm -rf " + folderDelete + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("removed folder!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// folder:
	// folderCreate:
	public boolean createFolder(String folder, String folderCreate) {
		try {
			String command = "cd " + folder + "\n mkdir " + folderCreate + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("created folder!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean downloadFile(String folder, String url) {
		try {
			String command = "cd " + folder + "\n wget " + url + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	// with command execute as admin (sudo) need password
	
	public boolean sudo() {
		try {
			String command = "sudo apt-get update";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y\n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean install(String namePackage) {
		try {
			// Ä‘á»ƒ thá»±c hiá»‡n nhiá»�u lá»‡nh: sau má»—i lá»‡nh ta thÃªm \n
			String command = "sudo apt-get install " + namePackage;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean uninstall(String namePackage) {
		try {

			String command = "sudo apt-get remove " + namePackage;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean getFileViaFtp(String serverFtp, String fileDownload,
			String folderStore) {
		try {

			String command = "cd "+folderStore+" \n wget ftp://"+serverFtp+"/"+fileDownload;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean getFileViaFtp(String serverFtp, String userFtp,
			String passwordFtp, String fileDownload, String folderStore) {
		try {

			String command = "cd "+folderStore+" \n wget ftp://"+userFtp+":"+passwordFtp+"@"+serverFtp+"/"+fileDownload;
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			((ChannelExec) channel).setPty(true);
			channel.setInputStream(null);
			channel.setOutputStream(System.out, true);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();

			channel.connect();
			out.write((pass + "\n").getBytes());
			// out.write(("y \n").getBytes());
			out.flush();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("DONE");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean createFolder(String folder) {
		try {
			String command = "mkdir " + folder + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("created folder!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean removeFolder(String folder) {
		try {
			String command = "rm -rf " + folder + "";
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, ipHost, 22);
			session.setPassword(pass);
			session.setConfig(config);
			session.connect();
			System.out.println("Connected");

			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			// ((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					System.out.println("exit-status: "
							+ channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
			System.out.println("removed folder!");

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public static String readStream(InputStream stream) {
		try {
			return IOUtils.toString(stream);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			if (stream != null)
				IOUtils.closeQuietly(stream);
		}
	}
}
