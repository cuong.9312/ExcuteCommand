package com.fis.cdd.remote.model;

public abstract class OperatingSystemBase {
	// account to login client
	String user;
	String pass;
	// ip target
	String ipHost;
	String status = "";

	public OperatingSystemBase() {
	}

	public OperatingSystemBase(String user, String pass, String ipHost) {
		this.user = user;
		this.pass = pass;
		this.ipHost = ipHost;
	}

	public abstract boolean createFolder(String dir);

	public abstract boolean removeFolder(String folder);

	public abstract boolean removeFile(String folder, String file);

	public abstract boolean getFileViaFtp(String serverFtp, String userFtp,
			String passwordFtp, String fileDownload, String folderStore);

	public abstract boolean getFileViaFtp(String serverFtp,
			String fileDownload, String folderStore);

	public abstract boolean runFileJar(String folder, String fileRun);

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getIpHost() {
		return ipHost;
	}

	public void setIpHost(String ipHost) {
		this.ipHost = ipHost;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
